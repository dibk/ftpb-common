﻿namespace simpleApp.Services.Application.BlobService
{
    public interface IBlobService
    {
        public  Task UploadBlobAsync(string blobName, Stream content);
        public  Task<Stream> DownloadBlobAsync(string blobName);
        public  Task<bool> DeleteBlobAsync(string blobName);
    }
}
