﻿using Azure.Storage.Blobs.Models;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Options;
using simpleApp.Services.Application.BlobService;

namespace simpleApp.Services.Application.BlobStorage
{
    public class StorageAccountSettings
    {
        public static string ConfigSection = "AzureStorage";
        public string ConnectionString { get; set; }
    }

    public class BlobService : IBlobService
    {
        private readonly string _connectionString;

        public BlobService(IOptions<StorageAccountSettings> options)
        {
            _connectionString = options.Value.ConnectionString;
        }

        public Task<bool> DeleteBlobAsync(string blobName)
        {
            throw new NotImplementedException();
        }

        public Task<Stream> DownloadBlobAsync(string blobName)
        {
            throw new NotImplementedException();
        }

        public Task UploadBlobAsync(string blobName, Stream content)
        {
            var container = GetBlobContainerClient(blobName);

            throw new NotImplementedException();
        }
        private BlobContainerClient GetBlobContainerClient(string containerName)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_connectionString);
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(containerName.ToLower());
            if (!containerClient.Exists())
                containerClient.CreateIfNotExists(PublicAccessType.None);

            return containerClient;
        }
    }
}