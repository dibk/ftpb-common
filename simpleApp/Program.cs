using Microsoft.EntityFrameworkCore;
using simpleApp.Models;
using simpleApp.Services.Application.ProductService;
using simpleApp.Services.Application.BlobService;
using simpleApp.Services.Infrastructure.Mailer;
using simpleApp.Services.Application.BlobStorage;

var builder = WebApplication.CreateBuilder(args); // <--- 1. Create the Builder (ASP.NET convention)

// SERVICES 
// default services added to the service container when creating 'web api project' template
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// adding a database service with some configuration
// -- dynamically insert connection string from appsettings.json (named DefaultConnection)
// -- this is registered with a Scoped lifetime 
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(@"Server=tcp:sqls-dibk-dev.database.windows.net;Authentication=Active Directory Default; Encrypt=True;Database=db-kjappkjipp"));

// adding a CRUD style service
builder.Services.AddTransient<IProductService, ProductService>();
builder.Services.AddHealthChecks();

// adding a CRUD style service
// -- these are almost always registered with transient lifetimes
//builder.Services.Configure<>(options => ConfigurationBinder.GetSection("BlobstorageConnectionString").Bind(options));
//builder.Services.AddTransient<IBlobService, BlobService>();

// adding an infrustructure-style service
// -- these are usually transient but can sometimes be scoped or singleton, usually the 3rd-party docs will specify
// builder.Services.AddTransient<IMailerService, MailChimpService>();


var app = builder.Build(); // <--- 2. Build the App (ASP.NET convention)



// MIDDLEWARE
// default middleware when choosing create 'web api project'
// Optional: add swagger when running in development mode 
app.UseSwagger();
app.UseSwaggerUI();


app.UseHttpsRedirection(); 
app.UseAuthorization(); 
app.MapControllers();
app.MapHealthChecks("/health");

app.Run(); // <--- 3. Run the App (ASP.NET convention)


