﻿using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Mvc;
using simpleApp.Services.Application.BlobService;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace simpleApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlobController : ControllerBase
    {
        public IBlobService _blobservice { get; }

        public BlobController(IBlobService blobservice)
        {
            _blobservice = blobservice;
        }

        // GET: api/<BlobController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<BlobController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<BlobController>
        [HttpPost]
        public async void Post([FromBody] string value)
        {
            using var stream = GenerateStreamFromString(value);
            await _blobservice.UploadBlobAsync("myContainer", stream);
        }

        // PUT api/<BlobController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<BlobController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
